package parser;

import java.util.LinkedList;
import lexer.Lexer;
import lexer.TokenType;
import lexer.Token;
import ast.SyntaxTree;
import ast.nodes.*;
import environment.Environment;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Implements a generic super class for parsing files.
 * @author Zach Kissel
 */
public class Parser
{
  private Lexer lex;            // The lexer for the parser.
  private boolean errorFound;   // True if ther was a parser error.
  private boolean doTracing;    // True if we should run parser tracing.
  private Token nextTok;        // The current token being analyzed.
  public SyntaxTree astCpy;

  /**
   * Constructs a new parser for the file {@code source} by
   * setting up lexer.
   * @param src the source code file to parse.
   * @throws FileNotFoundException if the file can not be found.
   */
  public Parser(File src) throws FileNotFoundException
  {
    lex = new Lexer(src);
    errorFound = false;
    doTracing = false;
  }

  /**
   * Construct a parser that parses the string {@code str}.
   * @param str the code to evaluate.
   */
  public Parser(String str)
  {
    lex = new Lexer(str);
    errorFound = false;
    doTracing = false;
  }

  /**
   * Turns tracing on an off.
   */
  public void toggleTracing()
  {
    doTracing = !doTracing;
  }

  /**
   * Determines if the program has any errors that would prevent
   * evaluation.
   * @return true if the program has syntax errors; otherwise, false.
   */
  public boolean hasError()
  {
    return errorFound;
  }

  /**
   * Parses the file according to the grammar.
   * @return the abstract syntax tree representing the parsed program.
   */
  public SyntaxTree parse()
  {    
    SyntaxTree ast;
    nextToken();    // Get the first token.
    ast = new SyntaxTree();
    ast = new SyntaxTree(evalProg(ast));   // Start processing at the root of the tree.

    if (nextTok.getType() != TokenType.EOF)
      logError("Parse error, unexpected token " + nextTok);
    return ast;
  }


  /************
   * Private Methods.
   *
   * It is important to remember that all of our non-terminal processing methods
   * maintain the invariant that each method leaves the next unprocessed token
   * in {@code nextTok}. This means each method can assume the value of
   * {@code nextTok} has not yet been processed when the method begins.
   ***********/

   /**
    * Method to handle the program non-terminal.
    *
    * <prog> -> <expr> { <expr> }
    */
    private SyntaxNode evalProg(SyntaxTree tree)
    {
      LinkedList<SyntaxNode> exprs = new LinkedList<>();

      trace("Enter <prog>");
      while (nextTok.getType() != TokenType.EOF)
      {
        exprs.add(evalFun(tree));
        
        if(errorFound)
            return null;
      }
      trace("Exit <prog>");
      return new ProgNode(exprs);
    }

  /**
   * Method to hand the fun non-terminal.
   *
   * <fun> -> fun <id> <lexpr> | <expr>
   */
   private SyntaxNode evalFun(SyntaxTree tree)
   {
     // Function definition.
     if (nextTok.getType() == TokenType.FUN)
     {
       nextToken();
       return handleFun(tree);
     }
     else // Just an expression.
     {
       return evalExpr(tree);
     }
   }

   /**
    * Method to handle the expression non-terminal
    *
    * <expr> -> let <id> := <expr> in <expr> |
    */
    private SyntaxNode evalExpr(SyntaxTree tree)
    {
        trace("Enter <expr>");
        SyntaxNode rexpr;
        TokenType op;
        SyntaxNode expr = null;

        // Handle let.
        if (nextTok.getType() == TokenType.LET)
        {
          nextToken();
          return handleLet(tree);
        }

        // Handle function application.
        else if (nextTok.getType() == TokenType.APPLY)
        {
          nextToken();
          return handleApply(tree);
        }

        // Handle conditionals.
        else if (nextTok.getType() == TokenType.IF)
        {
          nextToken();
          return handleIf(tree);
        }

        // Boolean not.
        else if (nextTok.getType() == TokenType.NOT)
        {
          op = nextTok.getType();
          nextToken();
          expr = evalRexpr(tree);
          expr = new UnaryOpNode(expr, op);
        }
        else  // and/or.
        {
          expr = evalRexpr(tree);

          while (nextTok.getType() == TokenType.AND ||
            nextTok.getType() == TokenType.OR)
          {
            op = nextTok.getType();
            nextToken();
            rexpr = evalRexpr(tree);
            expr = new BinOpNode(expr, op, rexpr);
          }
        }

        trace("Exit <expr>");

        return expr;
    }


    /**
     * Handles relational expressions.
     * rexpr ->mexpr [ ( **<** | **>** | **>=** | **<=** | **=** ) mexpr ]
     * @return a SyntaxNode representing the relation expression.
     */
    private SyntaxNode evalRexpr(SyntaxTree tree)
    {
      SyntaxNode left = null;
      SyntaxNode right = null;
      TokenType op;

      left = evalMexpr(tree);

      if (nextTok.getType() == TokenType.LT || nextTok.getType() == TokenType.LTE ||
          nextTok.getType() == TokenType.GT || nextTok.getType() == TokenType.GTE ||
          nextTok.getType() == TokenType.EQ || nextTok.getType() == TokenType.NEQ)
      {
          op = nextTok.getType();
          nextToken();
          right = evalMexpr(tree);
          return new RelOpNode(left, op, right);
      }

      return left;
    }


    /**
     * Handles the start of the matematics expressions.
     * mexpr -> <term> {(+ | - ) <term>}
     * @return a SyntaxNode representing the expression.
     */
    private SyntaxNode evalMexpr(SyntaxTree tree)
    {
      SyntaxNode expr = null;
      SyntaxNode rterm = null;
      TokenType op;
      expr = evalTerm(tree);

      while (nextTok.getType() == TokenType.ADD ||
        nextTok.getType() == TokenType.SUB)
      {
        op = nextTok.getType();
        nextToken();
        rterm = evalTerm(tree);
        expr = new BinOpNode(expr, op, rterm);
      }

      return expr;
    }

    
    /**
     * This method handles a function definition.
     * <id> <id> => <expr>
     * @return a function node.
     */
     private SyntaxNode handleLambda(SyntaxTree tree)
     {
      trace("Enter <handleLambda>");
      Token var;
      SyntaxNode expr;
      if (nextTok.getType() == TokenType.LAMBDA)
      {
          evalLambdaExpr(tree);
      }
      else
      {
        logError("Lambda expressions require parameters.");
      }
      return null;
     }
     
    /**
     * This method handles a function definition.
     * <id> <id> => <expr>
     * @return a function node.
     */
     private SyntaxNode handleFun(SyntaxTree tree)
     {
       Token funName;
       SyntaxNode lexpr;
       trace("Enter <handleFun>");

       if (nextTok.getType() == TokenType.ID)
       {
         funName = nextTok;
         nextToken();
         if(nextTok.getType() == TokenType.LAMBDA)
         {
             nextToken();

            lexpr = handleInsideLambda(tree);
            return new FunNode(funName, lexpr);
         }

       }
       else
       {
         logError("Functions need names.");
       }
       return null;
     }
     
    private SyntaxNode evalLambdaExpr(SyntaxTree tree)
    {
       trace("Enter <evalLambdaExpr>");
      Token var;
      SyntaxNode expr;
      if (nextTok.getType() == TokenType.ID)
      {
        var = nextTok;
        nextToken();
        if (nextTok.getType() == TokenType.TO)
        {
          nextToken();
          expr = evalExpr(tree);
          return new LambdaNode(var, expr,tree.getEnvironment().copy());
        }
        else
        {
          logError("Expected =>.");
        }
      }
      else
      {
        logError("Lambda expressions require parameters.");
      }
      return null;
    }
    
    /**
     * This method handles function application.
     * apply <id> <expr>
     */
    private SyntaxNode handleApply(SyntaxTree tree)
    {
      trace("Enter <handleApply>");
      Token fun;
      SyntaxNode expr;

      // Process a function identifier.
      if (nextTok.getType() == TokenType.ID)
      {
        fun = nextTok;
        nextToken();
        if(nextTok.getType() == TokenType.LAMBDA)
        {
            //Applying a lambda case
            nextToken();
            expr = handleInsideLambda(tree);
            return new ApplyNode(new TokenNode(fun), expr);

        }
        expr = evalExpr(tree);
        return new ApplyNode(new TokenNode(fun), expr);
      }
      // Process a lambda expression.
      else if (nextTok.getType() == TokenType.LAMBDA)
      {
        nextToken();
        SyntaxNode lexpr = handleInsideLambda(tree);
        return afterLambda(tree,lexpr);
      }
      else
      {
        logError("Function name or lambda expressoin expected.");
      }
      return null;
    }

    private SyntaxNode afterLambda(SyntaxTree tree, SyntaxNode lexpr)
    {
        trace("Enter <afterLambda>");
        Token id = nextTok;
        SyntaxNode expr;
        expr = evalExpr(tree);
        if(lexpr instanceof LambdaNode)
        {
         switch (id.getType()) {    //After a lambda expression is used, we must update the closure (Apply a value to the lambda) In order for the lambda to evaluate, it must get that info in its closure.
            case INT: 
                ((LambdaNode) lexpr).updateClosure(Integer.valueOf(id.getValue()));// When evaluating a lambda, set the closure of the lambda with the proper value
                     break;
            case REAL: 
                ((LambdaNode) lexpr).updateClosure(Double.valueOf(id.getValue()));
                     break;
            case CHAR:
                ((LambdaNode) lexpr).updateClosure(id.getValue().charAt(0));
                     break;
            case TRUE:
                ((LambdaNode) lexpr).updateClosure(true);
                     break;
            case FALSE:
                ((LambdaNode) lexpr).updateClosure(false);
                     break;
            case LIST:
                ((LambdaNode) lexpr).updateClosure(id);
                     break;
            default:  
                return null;
           }
        }
        return new ApplyNode(lexpr, expr);
    }
    /* Handles a lambda inside parens LAMBDA ( x => x + 5 ) */
    private SyntaxNode handleInsideLambda(SyntaxTree tree)
    {
      trace("Enter <handleInsideLambda>");
      if (nextTok.getType() == TokenType.LPAREN)
      {
        nextToken();
        SyntaxNode lexpr = evalLambdaExpr(tree);
        if (nextTok.getType() != TokenType.RPAREN)
        {
          logError("Closing paren expected.");
          return null;
        }
        nextToken();
        return lexpr;
      }
      return null;
    }
    /**
     * This method handles conditionals.
     * if <expr> then <expr> else <expr>
     */
    private SyntaxNode handleIf(SyntaxTree tree)
    {

      SyntaxNode cond;
      SyntaxNode trueBranch;
      SyntaxNode falseBranch;

      cond = evalExpr(tree);

      if (nextTok.getType() == TokenType.THEN)
      {
        nextToken();
        trueBranch = evalExpr(tree);
        if (nextTok.getType() == TokenType.ELSE)
        {
          nextToken();
          falseBranch = evalExpr(tree);
          return new IfNode(cond, trueBranch, falseBranch);
        }
        else
        {
          logError("Else expected.");
        }
      }
      else
      {
        logError("Expected then.");
      }
      return null;
    }

    /**
     * This method handles a let expression
     * <id> := <expr> in <expr>
     * @return a let node.
     */
    private SyntaxNode handleLet(SyntaxTree tree)
    {
        Token var = null;
        SyntaxNode varExpr = null;
        SyntaxNode expr = null;
        ArrayList<Token> multVals = new ArrayList<Token>();
        ArrayList<SyntaxNode> multVars = new ArrayList<SyntaxNode>();

        boolean moreThanOneVal = false;
        trace("enter handleLet");

        // Handle the identifier.
        if (nextTok.getType() == TokenType.ID || nextTok.getType() == TokenType.REAL ||
           nextTok.getType() == TokenType.TRUE || nextTok.getType() == TokenType.FALSE || nextTok.getType() == TokenType.CHAR )
        {
          var = nextTok;
          nextToken();
          
          if(nextTok.getType() == TokenType.COMMA)// If we have let x,y := ..., handle it via handleMultipleDeclaration()
          {
              multVals = handleMultipleDeclaration(var); //ArrayList<Token>, LHS
              moreThanOneVal = true;
          }
          

          // Handle the assignemnt.
          if (nextTok.getType() == TokenType.ASSIGN)
          {
            nextToken();
            if(moreThanOneVal)
                 multVars = evalMulti(tree, multVals.size());//ArrayList<SyntaxNodes>, RHS
            else
                 varExpr = evalExpr(tree);

            // Handle the in expr.
            if (nextTok.getType() == TokenType.IN)
            {
              nextToken();
              expr = evalExpr(tree);
              if(moreThanOneVal)
              {
                    return new LetNode(multVals, multVars, expr);
              }
              else
              {
                    return new LetNode(var, varExpr, expr);
              }
            }
            else
            {
              logError("Let expression expected in, saw " + nextTok + ".");
            }
          }
          else
          {
            logError("Let expression missing assignment!");
          }

        }
        else
          logError("Let expression missing variable.");
        trace("exit handleLet");
        return null;
    }
    
    /* Builds a token with multiple values in it, eg x,y,z
    */
    private ArrayList<Token> handleMultipleDeclaration(Token lastToken)
    {
        Token var = null;
        ArrayList<Token> retList = new ArrayList<Token>();
        trace("enter handleMultipleDeclaration");
        retList.add(lastToken);
        boolean stop = false;
        // nextTok is COMMA
        while(!stop)
        {
            if (nextTok.getType() == TokenType.ID)
            {
                retList.add(nextTok);
            } 
            nextToken(); 
            
            if(nextTok.getType() == TokenType.EOF)
            {
               logError("Let expression missing assignment!");
            }
            else if ( nextTok.getType() == TokenType.ASSIGN)
            {
                stop = true;
            }
        };
        trace("exit handleMultipleDeclaration");
        return retList;
    }

    /**
     * Method to handle the listExpr non-terminal.
     *
     * <listExpr> -> list ( [ (id | num) {, (id | num)}] )
     */
     private SyntaxNode evalListExpr()
     {
       LinkedList<TokenNode> entries = new LinkedList<>();
       ListNode lst = null;

       trace("Enter <listExpr>");

       if (nextTok.getType() == TokenType.LIST)
       {
         nextToken();
         if (nextTok.getType() == TokenType.LPAREN)
         {
            nextToken();

            // We could have an empty list.
            if (nextTok.getType() == TokenType.RPAREN)
            {
              lst = new ListNode(entries);
              nextToken();
              return lst;
            }
            if (nextTok.getType() == TokenType.INT ||
                nextTok.getType() == TokenType.REAL ||
                nextTok.getType() == TokenType.ID ||
                nextTok.getType() == TokenType.CHAR)
                  entries.add(new TokenNode(nextTok));
            else {
                logError("Invalid list element.");
                return new ListNode(entries);
            }
            nextToken();
            while (nextTok.getType() == TokenType.COMMA)
            {
              nextToken();
              if (nextTok.getType() == TokenType.INT ||
                  nextTok.getType() == TokenType.REAL ||
                  nextTok.getType() == TokenType.ID ||
                  nextTok.getType() == TokenType.CHAR)
                    entries.add(new TokenNode(nextTok));
              else {
                  logError("Invalid list element.");
                  return new ListNode(entries);
              }
              nextToken();
            }

            // Handle the end of the list.
            if (nextTok.getType() == TokenType.RPAREN)
            {
              lst = new ListNode(entries);
              nextToken();
            }
            else
            {
              logError("Invalid List");
            }
         }
         else
         {
            logError("Left paren expected.");
         }
       }
       else
       {
         logError("Unexpected list expression.");
       }

       trace("Exit <listExpr>");
       return lst;
     }

    /**
     * Method to handle the term non-terminal.
     *
     * <term> -> <factor> {( * | /) <factor>}
     */
     private SyntaxNode evalTerm(SyntaxTree tree)
     {
       SyntaxNode rfact;
       TokenType op;
       SyntaxNode term;

       trace("Enter <term>");
       term = evalFactor(tree);

       while (nextTok.getType() == TokenType.MULT ||
           nextTok.getType() == TokenType.DIV ||
           nextTok.getType() == TokenType.CONCAT)
       {
         op = nextTok.getType();
         nextToken();
         rfact = evalFactor(tree);
         term = new BinOpNode(term, op, rfact);
       }
       trace("Exit <term>");
       return term;
     }

     /**
      * Method to handle the factor non-terminal.
      *
      * <factor> -> <id> | <int> | <real> | ( <expr> )
      *             | list ( [ (id | num) {, (id | num)}])
      *             | true | false
      */
      private SyntaxNode evalFactor(SyntaxTree tree)
      {
        trace("Enter <factor>");
        SyntaxNode fact = null;

        if (nextTok.getType() == TokenType.ID ||
            nextTok.getType() == TokenType.INT ||
            nextTok.getType() == TokenType.REAL ||
            nextTok.getType() == TokenType.TRUE ||
            nextTok.getType() == TokenType.FALSE ||
            nextTok.getType() == TokenType.CHAR)
        {
            fact = new TokenNode(nextTok);
            nextToken();
        }
        else if (nextTok.getType() == TokenType.LIST)
        {
          return evalListExpr();
        }
        else if (nextTok.getType() == TokenType.LST_HD)
        {
          nextToken();
          fact = evalFactor(tree);
          return new HeadNode(fact);
        }
        else if (nextTok.getType() == TokenType.LST_TL)
        {
          nextToken();
          fact = evalFactor(tree);
          return new TailNode(fact);
        }
        else if (nextTok.getType() == TokenType.LPAREN)
        {
          nextToken();
          fact = evalExpr(tree);

          if (nextTok.getType() == TokenType.RPAREN)
            nextToken();
          else
            logError("Expected \")\" received " + nextTok +".");
        }
        else
        {
          logError("Unexpected token " + nextTok);

          // Recover from poorly formed expression.
          // if (nextTok.getType() == TokenType.RPAREN)
          //   nextToken();
        }

        trace("Exit <factor>");
        
        return fact;
      }
    /* Evaluates multiple declared variables ( RHS of a let statement with multiple names) */
    private ArrayList<SyntaxNode> evalMulti(SyntaxTree tree, int size)
    {
       trace("Enter <evalMulti>");
        SyntaxNode fact = null;
        ArrayList<SyntaxNode> assignments = new ArrayList<SyntaxNode>();
        boolean stop = false;
        // nextTok is COMMA
        while(!stop)
        {
            
            if (nextTok.getType() == TokenType.INT || nextTok.getType() == TokenType.REAL ||
                  nextTok.getType() == TokenType.TRUE ||nextTok.getType() == TokenType.FALSE ||
                  nextTok.getType() == TokenType.CHAR)
            {
                assignments.add(evalExpr(tree));
                
                if(nextTok.getType() != TokenType.IN)
                {
                                    nextToken(); 

                }
                if (nextTok.getType() == TokenType.COMMA)
                {
                     nextToken(); 
                } 
                if(nextTok.getType() == TokenType.EOF)
                {
                   logError("Let expression missing assignment!");
                   stop = true;
                }
                else if ( nextTok.getType() == TokenType.IN)
                {
                    stop = true;
                }
            }
            else if (nextTok.getType() == TokenType.LIST)
            {
                assignments.add(evalListExpr());
            }
            if (nextTok.getType() == TokenType.COMMA)
            {
                 nextToken(); 
            } 
            if(nextTok.getType() == TokenType.EOF)
            {
               logError("Let expression missing assignment!");
               stop = true;
            }
            else if ( nextTok.getType() == TokenType.IN)
            {
                stop = true;
            }
        };

        trace("Exit <factor>");
        
        if(assignments.size() == size)
        {
            return assignments;
        }
        logError("Let expressions argument arity error");
        return null;
    }

  /**
   * Logs an error to the console.
   * @param msg the error message to dispaly.
   */
   private void logError(String msg)
   {
     System.err.println("Error (" + lex.getLineNumber() + "): " + msg);
     errorFound = true;
   }

   /**
    * This prints a message to the screen on if {@code doTracing} is
    * true.
    * @param msg the message to display to the screen.
    */
    private void trace(String msg)
    {
      if (doTracing)
        System.out.println(msg);
    }

    /**
     * Gets the next token from the lexer potentially logging that
     * token to the screen.
     */
    private void nextToken()
    {
      nextTok = lex.nextToken();

      if (doTracing)
        System.out.println("nextToken: " + nextTok);

    }

}
