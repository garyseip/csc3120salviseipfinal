package ast.nodes;

import lexer.Token;
import lexer.TokenType;
import environment.Environment;
import java.util.LinkedList;

/**
 * This node represents relational operations.
 * @author Zach Kissel
 */
 public class RelOpNode extends SyntaxNode
 {
   private TokenType op;
   private SyntaxNode leftExpr;
   private SyntaxNode rightExpr;

   /**
    * Constructs a new binary operation syntax node.
    * @param lexpr the left operand.
    * @param op the binary operation to perform.
    * @param rexpr the right operand.
    */
    public RelOpNode(SyntaxNode lexpr, TokenType op, SyntaxNode rexpr)
    {
      this.op = op;
      this.leftExpr = lexpr;
      this.rightExpr = rexpr;
    }

    /**
     * Evaluate the node.
     * @param env the executional environment we should evaluate the
     * node under.
     * @return the object representing the result of the evaluation.
     */
     public Object evaluate(Environment env)
     {
        Object lval;
        Object rval;
        boolean useDouble = false, useChar = false, useList = false;

        lval = leftExpr.evaluate(env);
        rval = rightExpr.evaluate(env);

        if (lval == null || rval == null)
          return null;

        // Make sure the type is sound.
        if(!(lval instanceof Integer || lval instanceof Double || lval instanceof Character || 
                lval instanceof LinkedList) 
                && !(rval instanceof Double || rval instanceof Integer || rval instanceof Character 
                    || rval instanceof LinkedList))
          return null;


        if (lval.getClass() !=  rval.getClass())
        {
          System.out.println("Error: mixed type expression.");
          return null;
        }

        if (lval instanceof LinkedList)
        {
          useList = true;
          
          if(((LinkedList)lval).element() instanceof Double)
              useDouble = true;
          else if(((LinkedList)lval).element() instanceof Character)
              useChar = true;
        }
        else if (lval instanceof Double)
          useDouble = true;
        else if (lval instanceof Character)
          useChar = true;
        

        // Calls the relevant list helper method vased on the element type.
        // Three seperate methods are used for ease of casting.
        if(useList)
        {
            if(useDouble)
                return listEvalDoub((LinkedList)lval, (LinkedList)rval);
            else if(useChar)
                return listEvalChar((LinkedList)lval, (LinkedList)rval);
            else
                return listEvalInt((LinkedList)lval, (LinkedList)rval);
        }
        
        // Perform the operation base on the type.
        switch(op)
        {
          case LT:
            if (useDouble)
              return (Double) lval < (Double) rval;
            else if (useChar)
              return (Character) lval < (Character)rval;
            return (Integer) lval < (Integer) rval;
          case LTE:
            if (useDouble)
              return (Double) lval <= (Double) rval;
            else if (useChar)
              return (Character) lval <= (Character)rval;
            return (Integer) lval <= (Integer) rval;
          case GT:
            if (useDouble)
              return (Double) lval > (Double) rval;
            else if (useChar)
              return (Character) lval > (Character)rval;
            return (Integer)lval > (Integer) rval;
          case GTE:
            if (useDouble)
              return (Double) lval >= (Double) rval;
            else if (useChar)
              return (Character) lval >= (Character)rval;
            return (Integer) lval >= (Integer) rval;
          case EQ:
            return lval.equals(rval);
          case NEQ:
            return !(lval.equals(rval));
          default:
            return null;
        }

     }

    /**
     * Helper method which lexicographically compares lists of integers.
     * 
     * @param lval The left list to compare.
     * @param rval The right list to compare.
     * @return A boolean value is returned depending on the comparison.
     */
    private Object listEvalInt(LinkedList lval, LinkedList rval)
    {
        // Gets the minimum size of the lists.
        int minSize = Math.min(lval.size(), rval.size());
        
        // If it is checking for equality, exits early if the lists are of different sizes.
        if(op == TokenType.EQ && lval.size() != rval.size())
            return false;
        
        Integer lcomp;
        Integer rcomp ;
        
        // For loop compares each entry against one another.
        for(int i = 0; i < minSize; i++)
        {
            lcomp = (Integer)lval.get(i);
            rcomp = (Integer)rval.get(i);
            
            switch(op)
            {
                case LT:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return lval.size() < rval.size();
                    }
                    else
                        return lcomp < rcomp;
                    break;
                case LTE:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return lval.size() <= rval.size();
                    }
                    else
                        return lcomp <= rcomp;
                    break;
                case GT:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return lval.size() > rval.size();
                    }
                    else
                        return lcomp > rcomp;
                    break;
                case GTE:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return lval.size() >= rval.size();
                    }
                    else
                        return lcomp >= rcomp;
                    break;
                case EQ:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return true;
                    }
                    else
                        return false;
                    break;
                case NEQ:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return false;
                    }
                    else
                        return true;
                    break;
                default:
                    return null;
            }
        }
        
        return null;
    }

    /**
     * Helper method which lexicographically compares lists of integers.
     * 
     * @param lval The left list to compare.
     * @param rval The right list to compare.
     * @return A boolean value is returned depending on the comparison.
     */
    private Object listEvalDoub(LinkedList lval, LinkedList rval)
    {
        // Gets the minimum size of the lists.
        int minSize = Math.min(lval.size(), rval.size());
        
        // If it is checking for equality, exits early if the lists are of different sizes.
        if(op == TokenType.EQ && lval.size() != rval.size())
            return false;
        
        Double lcomp;
        Double rcomp ;
        
        // For loop compares each entry against one another.
        for(int i = 0; i < minSize; i++)
        {
            lcomp = (Double)lval.get(i);
            rcomp = (Double)rval.get(i);
            
            switch(op)
            {
                case LT:
                    if(lcomp.compareTo(rcomp) == 0)
                    {
                        if(i == minSize - 1)
                            return lval.size() < rval.size();
                    }
                    else
                        return lcomp < rcomp;
                    break;
                case LTE:
                    if(lcomp.compareTo(rcomp) == 0)
                    {
                        if(i == minSize - 1)
                            return lval.size() <= rval.size();
                    }
                    else
                        return lcomp <= rcomp;
                    break;
                case GT:
                    if(lcomp.compareTo(rcomp) == 0)
                    {
                        if(i == minSize - 1)
                            return lval.size() > rval.size();
                    }
                    else
                        return lcomp > rcomp;
                    break;
                case GTE:
                    if(lcomp.compareTo(rcomp) == 0)
                    {
                        if(i == minSize - 1)
                            return lval.size() >= rval.size();
                    }
                    else
                        return lcomp >= rcomp;
                    break;
                case EQ:
                    if(lcomp.compareTo(rcomp) == 0)
                    {
                        if(i == minSize - 1)
                            return true;
                    }
                    else
                        return false;
                    break;
                case NEQ:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return false;
                    }
                    else
                        return true;
                    break;
                default:
                    return null;
            }
        }
        
        return null;
    }

    /**
     * Helper method which lexicographically compares lists of integers.
     * 
     * @param lval The left list to compare.
     * @param rval The right list to compare.
     * @return A boolean value is returned depending on the comparison.
     */
    private Object listEvalChar(LinkedList lval, LinkedList rval)
    {
        // Gets the minimum size of the lists.
        int minSize = Math.min(lval.size(), rval.size());
        
        // If it is checking for equality, exits early if the lists are of different sizes.
        if(op == TokenType.EQ && lval.size() != rval.size())
            return false;
        
        Character lcomp;
        Character rcomp ;
        
        // For loop compares each entry against one another.
        for(int i = 0; i < minSize; i++)
        {
            lcomp = (Character)lval.get(i);
            rcomp = (Character)rval.get(i);
            
            if(Character.isLetter(lcomp) && Character.isLetter(rcomp))
            {
                lcomp = Character.toLowerCase(lcomp);
                rcomp = Character.toLowerCase(rcomp);
            }
                
            
            switch(op)
            {
                case LT:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return lval.size() < rval.size();
                    }
                    else
                        return lcomp < rcomp;
                    break;
                case LTE:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return lval.size() <= rval.size();
                    }
                    else
                        return lcomp <= rcomp;
                    break;
                case GT:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return lval.size() > rval.size();
                    }
                    else
                        return lcomp > rcomp;
                    break;
                case GTE:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return lval.size() >= rval.size();
                    }
                    else
                        return lcomp >= rcomp;
                    break;
                case EQ:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return true;
                    }
                    else
                        return false;
                    break;
                case NEQ:
                    if(lcomp == rcomp)
                    {
                        if(i == minSize - 1)
                            return false;
                    }
                    else
                        return true;
                    break;
                default:
                    return null;
            }
        }
        
        return null;
    }
 }
