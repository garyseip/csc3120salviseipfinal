package ast.nodes;

import lexer.Token;
import ast.nodes.LambdaNode;

import lexer.TokenType;
import environment.Environment;

/**
 * This node represents the unary op node.
 * @author Zach Kissel
 */
 public class ApplyNode extends SyntaxNode
 {
   private SyntaxNode func;
   private SyntaxNode arg;

   /**
    * Constructs a new node that represents function application.
    * @param func the function to apply.
    * @param arg the argument to apply the function to.
    */
   public ApplyNode(SyntaxNode func,  SyntaxNode arg)
   {
     this.func = func;
     this.arg = arg;
   }
   
   /*Gets the argument being applied to a function, used in appling a lamba to a function */
   public SyntaxNode getArg()
   {
       return this.arg;
   }

   /**
    * Evaluate the node.
    * @param env the executional environment we should evaluate the
    * node under.
    * @return the object representing the result of the evaluation.
    */
   public Object evaluate(Environment env)
   {
     SyntaxNode node = null;
     LambdaNode lexp;

     // If our function is a TokenNode we should evaluate it as it
     // may be an identifier.
     if (func instanceof TokenNode)
     {
        TokenNode n = (TokenNode)func;
        node = (SyntaxNode)func.evaluate(env);
     }

     // Make sure we have a function to apply.
     if (!(node instanceof FunNode) && !(func instanceof LambdaNode))
     {
       System.out.println("Apply not given a function.");
       return null;
     }

     // Get the lambda expression to evaluate.
     if (node instanceof FunNode)
     {
       FunNode function = (FunNode) node;
       lexp = (LambdaNode)function.getLambdaExpression();
     }
     else
      lexp = (LambdaNode)func;

      // Bind the parameter to the argument and evaluate the function call.
      
     Environment newEnv = env.copy();   //Make a copy of the environment at the moment
     newEnv.updateEnvironment(lexp.getVar(), arg.evaluate(env));    //update the environment with the value of the lambda expression currently
     
     if(arg instanceof LambdaNode) // we are applying a lambda to a function.
     {
         FunNode f = (FunNode)func.evaluate(env);//get the function we are applying this to.
         LambdaNode l = (LambdaNode)f.getLambdaExpression();//open it up and get its lambda
         ApplyNode a = (ApplyNode)l.getExpr();//In order for it to use the first order lambda, it must apply some value to it, get the apply value.
         
         LambdaNode ourClosure = (LambdaNode)arg;//get our lambda, update its closure with the functions value
         newEnv.updateEnvironment(ourClosure.getVar(), a.getArg().evaluate(newEnv));//update the newEnv variable with the new situation.
         
         LambdaNode temp = (LambdaNode)arg;
         temp.updateClosure(newEnv);
         return arg.evaluate(newEnv);
     }
     lexp.updateClosure(newEnv);    //Give the lambda expression/closure the value currently.
     return lexp.evaluate(env);
   }
 }
