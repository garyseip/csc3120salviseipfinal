package ast.nodes;

import lexer.Token;
import lexer.TokenType;
import environment.Environment;

/**
 * This node represents a lambda expression.
 * @author Zach Kissel
 */
 public class LambdaNode extends SyntaxNode
 {
   private Token var;
   private SyntaxNode expr;
   private Environment closure;

   /**
    * Constructs a new function node which represents
    * a function declaration.
    * @param var the free variable in the expression.
    * @param expr the expression to excute.
    */
   public LambdaNode(Token var, SyntaxNode expr, Environment closure)
   {
     this.var = var;
     this.expr = expr;
     this.closure = closure;
   }

  /**
   * Get the parameter of the function.
   * @return a Token representing the parameter name.
   */
   public Token getVar()
   {
     return var;
   }
   /*Gets the expression associated with a lambdaNode, used when we apply a lambda to a function.*/
   public SyntaxNode getExpr()
   {
     return expr;
   }
   /* Method to update the current environment/closure with a specific value */
   public void updateClosure(Object val)
   {
     closure.updateEnvironment(var, val);
   }
      /* Method to update the current environment/closure with a specific environment */
   public void updateClosure(Environment newEnv)
   {
       closure = newEnv;
   }

   /**
    * Evaluate the node.
    * @param env the executional environment we should evaluate the
    * node under.
    * @return the object representing the result of the evaluation.
    */
   public Object evaluate(Environment env)
   { 
     return expr.evaluate(this.closure);
   }
 }
