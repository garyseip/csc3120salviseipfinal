package ast.nodes;

import lexer.Token;
import lexer.TokenType;
import environment.Environment;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * This node represents a let expression.
 * @author Zach Kissel
 */
 public class LetNode extends SyntaxNode
 {
   private Token var;
   private SyntaxNode varExpr;
   private SyntaxNode expr;
   private ArrayList<Token> vars = null;
   private ArrayList<SyntaxNode> varsExpr = null;
   private boolean mult;


   /**
    * Constructs a new binary operation syntax node.
    * @param var the variable identifier.
    * @param varExpr the expression that give the varaible value.
    * @param expr the expression that uses the variables value.
    */
    public LetNode(Token var, SyntaxNode varExpr, SyntaxNode expr)
    {
      this.var = var;
      this.varExpr = varExpr;
      this.expr = expr;
      this.mult = false;
    }
    public LetNode(ArrayList<Token> vars,  ArrayList<SyntaxNode> varExpr, SyntaxNode expr)
    {
      this.vars = vars;
      this.varsExpr = varExpr;
      this.expr = expr;
      this.mult = true;
    }

    /**
     * Evaluate the node.
     * @param env the executional environment we should evaluate the
     * node under.
     * @return the object representing the result of the evaluation.
     */
     public Object evaluate(Environment env)
     {
       if(mult == false){
        Object varVal = varExpr.evaluate(env);

        if (varVal instanceof Integer || varVal instanceof Double ||
            varVal instanceof LinkedList || varVal instanceof Character)
          env.updateEnvironment(var, varVal);
        else
          System.out.println("Failed to add " + var + "with  value " + varVal.getClass());

        Object value = expr.evaluate(env);
        return value;
      }
      else  /*If mult == true, we have several arguements to procesess and evaluate */
      {
          for(int i = 0; i < this.vars.size(); i++)
          {
             Object varVal = varsExpr.get(i).evaluate(env); 
             env.updateEnvironment(vars.get(i), varVal);
          }
          Object value = expr.evaluate(env);
          return value;
      }
     }
 }
