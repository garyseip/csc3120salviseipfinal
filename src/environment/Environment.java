package environment;

import lexer.Token;
import lexer.TokenType;
import java.util.HashMap;

/**
 * A simple representation of an executional environment.
 * @author Zach Kissel
 */
public class Environment
{
  // globals holds all global variables. Its existence only increases the space cost by a single
  // HashMap.
  private HashMap<String, Object> env, globals;

  /**
   * Sets up the initial environment.
   */
  public Environment()
  {
    env = new HashMap<>();
    globals = new HashMap<>();
  }

  /**
   * Returns the evironment value associated with a token.
   * @param tok the token to look up the value of.
   * @return the value of {@code tok} in the environment. A value of null
   * is returned if hte token is not in the environment.
   */
  public Object lookup(Token tok)
  {
      HashMap toLook;
      if(tok.getValue().startsWith("gbl "))
        toLook = globals;
      else
        toLook = env;
      
      return toLook.get(tok.getValue());
  }

  /**
   * Update the environment such that token {@code tok} has
   * the given value {@code val}.
   * @param tok the token to update.
   * @param val the value to associate with the token.
   */
  public void updateEnvironment(Token tok, Object val)
  {
    // toChange stores which HashMap to change.
    HashMap toChange;  
    if(tok.getValue().startsWith("gbl "))
        toChange = globals;
    else
        toChange = env;
      
    // Modifies the relevant HashMap.
    if (toChange.replace(tok.getValue(), val) == null)
        toChange.put(tok.getValue(), val);
  }

  /**
   * Makes a copy of the current environment.
   * @return a copy of the environment.
   */
  public Environment copy()
  {
    Environment newEnv = new Environment();
    newEnv.env.putAll(env);
    
    // Simply sets the global variabls of the copy to the current global variabls.
    newEnv.globals = globals;
    return newEnv;
  }

  /**
   * Provides a string representing the environment.
   * @return a string representation of the environment.
   */
  @Override
  public String toString()
  {
    return env.toString();
  }
}
